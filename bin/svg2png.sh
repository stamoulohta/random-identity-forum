#! /usr/bin/env bash

for f in *.svg; do
    inkscape -z -e "${f%.*}.png" -w 1000 -h 600 "$f"
done;