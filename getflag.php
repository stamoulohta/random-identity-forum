<?php
const FLAG_W = 1000;
const FLAG_H = 600;
const STEP_X = 50;
const STEP_Y = 60;
const FACTOR = 5;                                                               // Common Factors (50, 60) : 1, 2, 5, 10

const INC_X = STEP_X / FACTOR;                                                  // width of pixel block;
const INC_Y = STEP_Y / FACTOR;                                                  // height of pixel block;
$pBlocks = ((FLAG_W * FLAG_H) / ((INC_X * INC_Y))) / 100;                       // pixel blocks per one percent

$flags = $blocks = [];

foreach ($_POST as $flag => $percent) {
    if (!$percent) {
        continue;
    }

    $flags[] = imagecreatefrompng("img/flags/png/{$flag}.png");
    $flag_i = count($flags) - 1;
    for ($p = 0; $p < $percent; $p++) {
        array_push($blocks, ...array_fill(0, $pBlocks, $flag_i));
    }
}
shuffle($blocks);

$res = imagecreatetruecolor(FLAG_W, FLAG_H);

$i = 0;
for ($y = 0; $y < FLAG_H; $y += INC_Y) {
    for ($x = 0; $x < FLAG_W; $x += INC_X) {
        imagecopy($res, $flags[$blocks[$i++]], $x, $y, $x, $y, INC_X, INC_Y);
    }
}

ob_start();
imagepng($res);
printf('data:image/png;base64,%s', base64_encode(ob_get_clean()));