<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Play RIF</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
        <meta charset="utf-8">
        <meta name="description" content="Combine EU flags to create your own">
        <meta name="keywords" content="europe,eu,game,flags,game,rif,random identity forum">
        <meta name="author" content="George Stamoulis">

        <meta property="og:title" content="Play RIF"/>
        <meta property="og:type" content="article"/>
        <meta property="og:site_name" content="Anna Lascari"/>
        <meta property="og:url" content="http://stamoulohta.com/temp/goplay.php"/>
        <meta property="og:description" content="Combine percentages of EU flags into one flag."/>
        <meta property="og:image" content="http://stamoulohta.com/temp/img/og.png"/>
        <meta property="og:image:type" content="image/png"/>
        <meta property="og:image:width" content="500"/>
        <meta property="og:image:height" content="300"/>
        <meta property="og:image:alt" content="Hypothetical EU flag by combining those of it's members"/>

        <meta name="twitter:card" content="summary"/>
        <meta name="twitter:site" content="@alphalamda14"/>

        <link rel="stylesheet" type="text/css" href="node_modules/bootstrap-4-grid/css/grid.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css?ver=20191022">
    </head>

    <body>
    <div class="container mw-100">
        <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-11 d-flex d-md-block flex-column align-items-center">
                <h1 class="d-inline-block mb-1">RIF</h1>
                <nav class="d-print-none d-inline-block">
                    <a class="ml-md-2 p-0" title="About.." href="index.html">a</a>
                    <a class="ml-1 p-0" title="How to Play" href="howtoplay.html">h</a>
                    <a class="ml-1 p-0 current" title="Go Play" href="#">p</a>
                </nav>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-sm-11 mt-3 mt-md-0">
                <form id="form-flags" action="getflag.php" method="post">
                    <div class="row d-flex overflow-auto justify-content-center select-element">
                        <div class="col-12 d-flex flex-column flex-md-row justify-content-between align-items-center">

                            <?php
                            $members = [
                                '', '', 'se', '', '', 'fi',
                                'ie', 'nl', 'dk', 'cz', 'pl', 'ee',
                                'gb', 'be', 'de', 'at', '', 'lv',
                                'fr', 'lu', 'it', 'si', 'sk', 'lt',
                                'es', '', '', 'hr', 'hu', 'ro',
                                'pt', '', '', '', 'gr', 'bg',
                                '', '', 'mt', '', '', 'cy'
                            ];

                            $titles = [
                                'se' => 'Sweden', 'fi' => 'Finland', 'ie' => 'Ireland', 'nl' => 'Netherlands', 'dk' => 'Denmark',
                                'cz' => 'Czech Republic', 'pl' => 'Poland', 'ee' => 'Estonia', 'gb' => 'United Kingdom', 'be' => 'Belgium',
                                'de' => 'Germany', 'at' => 'Austria', 'lv' => 'Latvia', 'fr' => 'France', 'lu' => 'Luxembourg',
                                'it' => 'Italy', 'si' => 'Slovenia', 'sk' => 'Slovakia ', 'lt' => 'Lithuania', 'es' => 'Spain',
                                'hr' => 'Croatia', 'hu' => 'Hungary', 'ro' => 'Romania', 'pt' => 'Portugal', 'gr' => 'Greece',
                                'bg' => 'Bulgaria', 'mt' => 'Malta', 'cy' => 'Cyprus'];

                            foreach ($members as $i => $member) {
                                if ($i && !($i % 6)) echo('</div><div class="col-12 d-flex flex-column flex-md-row justify-content-between align-items-center">');
                                if ($member) {
                                    $content = sprintf('<img title="%2$s" id="flag-%1$s" class="flag" alt="Flag of %1$s" src="img/flags/svg/%1$s.svg"><div class="d-flex justify-content-left w-100" ><input class="percent mt-0 w-100" type="number" placeholder="%%" min="0" max="100" name="%1$s" id="%1$s"></div>', $member, $titles[$member]);
                                } else {
                                    $content = null;
                                }
                                printf('<div class="div-flag %s d-md-flex flex-row flex-md-column mb-3 p-0">%s</div>', $content ? 'd-flex' : 'd-none', $content);
                            }
                            ?>

                        </div>
                    </div>

                    <div class="row justify-content-center result-element">
                        <img id="result" class="col-md-12" src="" alt="Combination flag">
                    </div>

                    <div class="d-print-none row d-flex justify-content-left mt-3">
                        <div class="col-12 col-md-9 d-flex flex-column flex-sm-row justify-content-center justify-content-md-start">
                            <input id="equal" class="to-anchor select-element pl-sm-0" type="button"
                                   value="EQUAL DISTRIBUTION">
                            <input id="clear" class="to-anchor select-element" type="button" value="CLEAR"
                                   title="Clear All Inputs">
                            <input id="mail" class="to-anchor result-element pl-sm-0" type="button" value="E-MAIL"
                                   title="Send Flag">
                            <input id="print" class="to-anchor result-element" type="button" value="PRINT"
                                   title="Print Flag">
                            <input id="return" class="to-anchor result-element" type="button" value="RETURN"
                                   title="Go Back">
                            <input id="submit" class="to-anchor d-flex" type="submit" value="SUBMIT" title="Show Flag">
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
    <footer>
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="js/script.js?ver=20191022"></script>
    </footer>
    </body>
</html>
