const $resElements = $(".result-element");
const $selectElements = $(".select-element");
const $percentages = $(".percent");

const _equal = function () {
    const values = (Array(12).fill(3)).concat(Array(16).fill(4)).sort(function () {
        return Math.random() - 0.5
    });
    $percentages.each(function (i, e) {
        $(e).val(values.pop());
    });
};

const _clear = function () {
    $percentages.val('');
};

const _return = function () {
    _show_result(false);
};

const _show_result = function (show) {
    $resElements.removeClass(show ? "d-none" : "d-flex");
    $resElements.addClass(show ? "d-flex" : "d-none");
    $selectElements.removeClass(show ? "d-flex" : "d-none");
    $selectElements.addClass(show ? "d-none" : "d-flex");
};

const _submit = function (e) {
    e.preventDefault();

    var total = 0;
    $.each($percentages, function (i, flag) {
        total += flag.value ? parseInt(flag.value) : 0;
    });

    if (total !== 100) {
        alert("Percentages add up to " + total + ". They must total 100.");
        return;
    }

    $.post(
        this.action,
        $(this).serialize(),
        function (data) {
            $("#result").attr("src", data);
            _show_result(true);
        }
    );
};

const _mail = function () {
    window.open("mailto:?subject=RIF&body=Copy paste image into this email.", "_blank");
};

const _print = function () {
    window.print();
};

const _blink = function () {
    $("#flag-gb").addClass("blink");
};

const _stop_blink = function () {
    $("#flag-gb").removeClass("blink");
};

const _insert = function () {
    $("#" + $(this).attr("id").slice(-2)).focus();
};

$(function () {
    $("#equal").on("click", _equal);
    $("#clear").on("click", _clear);
    $("#form-flags").on("submit", _submit);
    $("#return").on("click", _return);
    $("#mail").on("click", _mail);
    $("#print").on("click", _print);
    $("#gb").on("focus", _blink)
        .on("focusout", _stop_blink);
    $(".flag").on("click", _insert);

    _show_result(false);
});
